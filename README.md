##Step 1. this will provide the check up,perhaps the some modules required need to install
##if you don't installed yet composer follow this link :
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-16-04
##then run commnand
##inside the folder of project run the commad: 
composer install
## in this step you should configure the symfony project just follow steps in command line
##Step 1.1 We need create the database before 

##inside the folder of project run the commad: 
./bin/console doctrine:database:create

##Step 2. create structure of the database 
##inside the folder of project run the commad: 
./bin/console doctrine:migration:migrate

##Step 3. You can create simple data from fixtures module for this case use the command:
##inside the folder of project run the commad: 
./bin/console doctrine:fixtures:load

##Step 4. This will start On-board server
##inside the folder of project run the commad: 
./bin/console server:run 

##Step 5. if you use Symfony On-Board server you use this routes for navigated in App

http://127.0.0.1:'your server port'/app_dev.php/list  
## List of all product
##(You can use search in /list url for searching products)

http://127.0.0.1:'your server port'/app_dev.php/product 
## admin-side product control
##if you delete the parent product 
##it will be deleted all related children from variableProducts

http://127.0.0.1:'your server port'/app_dev.php/variable/ 
##admin-side variable product control

##"for adding variable product to parent product you should enter inside the variable product (edit button or if you create a new product ) and
##choice the parent product from the drop down" <<<Attention! parent product should be already created>>>
## if you added the children product to parent child product will be show inside the parent product in "Child Products" block

http://127.0.0.1:'your server port'/app_dev.php/user/ 
## admin-side user control



