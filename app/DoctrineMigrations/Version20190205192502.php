<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190205192502 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vat (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(45) DEFAULT NULL, value NUMERIC(10, 0) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE variable_product (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, name VARCHAR(45) DEFAULT NULL, price VARCHAR(45) DEFAULT NULL, description VARCHAR(45) DEFAULT NULL, sku VARCHAR(45) DEFAULT NULL, INDEX fk_variable_product_product1_idx (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(45) DEFAULT NULL, password VARCHAR(45) DEFAULT NULL, password_salt VARBINARY(255) DEFAULT NULL, UNIQUE INDEX email_UNIQUE (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(45) DEFAULT NULL, price NUMERIC(10, 0) DEFAULT NULL, sku VARCHAR(45) DEFAULT NULL, description TEXT DEFAULT NULL, date_create DATE DEFAULT NULL, Category_id INT DEFAULT NULL, Vat_id INT DEFAULT NULL, INDEX fk_Product_Vat_idx (Vat_id), INDEX fk_Product_Category1_idx (Category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_image (id INT AUTO_INCREMENT NOT NULL, Product_id INT DEFAULT NULL, INDEX fk_product_image_Product1_idx (Product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(45) DEFAULT NULL, slug VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE variable_product ADD CONSTRAINT FK_BDAEBDCF4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD94DA1235 FOREIGN KEY (Category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADB21A3F5D FOREIGN KEY (Vat_id) REFERENCES vat (id)');
        $this->addSql('ALTER TABLE product_image ADD CONSTRAINT FK_64617F03AD9658A FOREIGN KEY (Product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADB21A3F5D');
        $this->addSql('ALTER TABLE variable_product DROP FOREIGN KEY FK_BDAEBDCF4584665A');
        $this->addSql('ALTER TABLE product_image DROP FOREIGN KEY FK_64617F03AD9658A');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD94DA1235');
        $this->addSql('DROP TABLE vat');
        $this->addSql('DROP TABLE variable_product');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_image');
        $this->addSql('DROP TABLE category');
    }
}
