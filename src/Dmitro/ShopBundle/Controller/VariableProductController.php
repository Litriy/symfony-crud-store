<?php

namespace Dmitro\ShopBundle\Controller;

use Dmitro\ShopBundle\Entity\VariableProduct;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Variableproduct controller.
 *
 * @Route("variable")
 */
class VariableProductController extends Controller
{
    /**
     * Lists all variableProduct entities.
     *
     * @Route("/", name="variable_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $variableProducts = $em->getRepository('DmitroShopBundle:VariableProduct')->findAll();

        return $this->render('variableproduct/index.html.twig', array(
            'variableProducts' => $variableProducts,
        ));
    }

    /**
     * Creates a new variableProduct entity.
     *
     * @Route("/new", name="variable_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $variableProduct = new Variableproduct();
        $form = $this->createForm('Dmitro\ShopBundle\Form\VariableProductType', $variableProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($variableProduct);
            $em->flush();

            return $this->redirectToRoute('variable_show', array('id' => $variableProduct->getId()));
        }

        return $this->render('variableproduct/new.html.twig', array(
            'variableProduct' => $variableProduct,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a variableProduct entity.
     *
     * @Route("/{id}/show", name="variable_show")
     * @Method("GET")
     */
    public function showAction(VariableProduct $variableProduct)
    {
        $deleteForm = $this->createDeleteForm($variableProduct);

        return $this->render('variableproduct/show.html.twig', array(
            'variableProduct' => $variableProduct,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing variableProduct entity.
     *
     * @Route("/{id}/edit", name="variable_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, VariableProduct $variableProduct)
    {
        $deleteForm = $this->createDeleteForm($variableProduct);
        $editForm = $this->createForm('Dmitro\ShopBundle\Form\VariableProductType', $variableProduct);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('variable_edit', array('id' => $variableProduct->getId()));
        }

        return $this->render('variableproduct/edit.html.twig', array(
            'variableProduct' => $variableProduct,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a variableProduct entity.
     *
     * @Route("/{id}", name="variable_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, VariableProduct $variableProduct)
    {
        $form = $this->createDeleteForm($variableProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($variableProduct);
            $em->flush();
        }

        return $this->redirectToRoute('variable_index');
    }

    /**
     * Creates a form to delete a variableProduct entity.
     *
     * @param VariableProduct $variableProduct The variableProduct entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(VariableProduct $variableProduct)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('variable_delete', array('id' => $variableProduct->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
