<?php

namespace Dmitro\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Pagerfanta\Adapter\DoctrineORMAdapter;

class ListProductController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/list", name="product_list")
     * @Method("GET")
     */
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository('DmitroShopBundle:Product')->findAll();

        return $this->render('product/listProduct.html.twig', array(
            'products' => $products,
        ));
    }

    /**
     * @Route("/search", name="handleSearch")
     * @Method("GET")
     */
    public function productSearchAction(Request $request)
    {
        $q = $request->query->get('q');
        $q = preg_split('/\s+/', $q);
        $dql = array();
        $em = $this->getDoctrine()->getManager();
        $search = $em->getRepository('DmitroShopBundle:Product');

        $qb = $search->createQueryBuilder('prod');
        foreach ($q as $i => $_q) {
            $dql[] = "prod.name LIKE :search$i";

            $qb->setParameter("search$i", '%' . $_q . '%');
        }
        $qb->andwhere('(' . implode(' OR ', $dql) . ')');

        $result = $qb->getQuery()->getResult();

        return $this->render(
            'product/listProduct.html.twig',
            array('productsSearch' => $result)
        );
    }

}


