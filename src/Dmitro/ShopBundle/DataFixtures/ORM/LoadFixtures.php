<?php
/**
 * Created by PhpStorm.
 * User: marcon
 * Date: 29.01.19
 * Time: 16:08
 */

namespace Dmitro\ShopBundle\DataFixtures\ORM;

use Dmitro\ShopBundle\Entity\Product;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;

class LoadFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        Fixtures::load(__DIR__ . '/fixtures.yml',
            $manager,
            [
                'providers' => [$this]
            ]

        );
    }

    public function product()
    {
        $genera = [
            'Lamp',
            'Notebook',
            'DVD',
            'TV',
            'Radio',
            'MP3-player',
            'USB',
            'Mobile',
            'PowerBank',
            'Headphones',
            'BlueTooth',
            'Desktop-PC',
            'remote-control',
            'Blender'
        ];
        $key = array_rand($genera);
        return $genera[$key];
    }

    public function category()
    {

        $genera = [
            'Home',
            'Office',
            'Street',
        ];

        $key = array_rand($genera);
        return $genera[$key];
    }

    public function variableProduct()
    {

        $genera = [
            'Red-Lamp',
            'Notebook-white',
            'DVD-black',
            'TV-yellow',
            'Radio-orange',
        ];

        $key = array_rand($genera);
        return $genera[$key];
    }
    public function vat()
    {

        $genera = [
            '',
            'General',
            'Reduced',
            'Special',
        ];

        $key = array_rand($genera);
        return $genera[$key];
    }



}