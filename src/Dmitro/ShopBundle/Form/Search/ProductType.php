<?php

namespace Dmitro\ShopBundle\Form\Search;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('search', null, array('required' => false, 'label' => 'Поиск '))
                ->add('category', null, array(
                'label' => 'Category',
                'required' => false,
                'class' => 'Dmitro\\ShopBundle\\Entity\\Category'));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'csrf_protection' => false,
        );
    }

    function getName()
    {
        return 'searchorg';
    }

}
